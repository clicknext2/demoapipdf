﻿using IronPdf.Signing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using PDFDemo3.Core.BusinessModels.LedgerBook;
using PDFDemo3.Core.BusinessModels.PurchaseOrder;
using PDFDemo3.SeedWorks;
using PDFDemo3.Services.Controllers;
using System.Reflection.PortableExecutable;

namespace PDFDemo3.Core.BusinessLogics.PurchaseOrder
{
    public class LedgerBookPdfManage
	{
        readonly IRazorViewToStringRenderer razorViewToStringRenderer;

        public LedgerBookPdfManage(IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            this.razorViewToStringRenderer = razorViewToStringRenderer;
        }
        public async Task<IActionResult> GeneratePdfLedgerBook(LedgerBookRequest request)
        {
            string PDFfilename = DateTime.Now.Ticks.ToString() + ".pdf";
            var headerHtml = await razorViewToStringRenderer.RenderViewToStringAsync("SeedWorks/Templates/LedgerBook/Header.cshtml", request);

            var renderer = new ChromePdfRenderer();
            renderer.RenderingOptions.MarginTop = 40;
            renderer.RenderingOptions.MarginLeft = 10;
            renderer.RenderingOptions.MarginRight = 10;
            renderer.RenderingOptions.MarginBottom = 41;
            renderer.RenderingOptions.HtmlHeader = new HtmlHeaderFooter()
            {
                HtmlFragment = headerHtml,
				MaxHeight = 50
			};

            renderer.RenderingOptions.PaperFit.UseResponsiveCssRendering(1100);

            var html = await razorViewToStringRenderer.RenderViewToStringAsync("SeedWorks/Templates/LedgerBook/LedgerBook.cshtml", request);
            var pdf = await renderer.RenderHtmlAsPdfAsync(html);


            if (request.Watermark != null)
            {
                string watermarkHtml = "<H1>" + request.Watermark + "</H1>";
                pdf.ApplyWatermark(watermarkHtml, rotation: -45, opacity: 50);
            }  

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "TempFilePDF", PDFfilename);
            pdf.SaveAs(filePath); 

            return await DownloadFile(PDFfilename);
        }
        private async Task<IActionResult> DownloadFile(string filename)
        {
            var Downloadfilepath = Path.Combine(Directory.GetCurrentDirectory(), "TempFilePDF", filename);

            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(Downloadfilepath, out var contenttype))
            {
                contenttype = "application/octet-stream";
            }

            var bytes = await System.IO.File.ReadAllBytesAsync(Downloadfilepath);
            return new FileContentResult(bytes, contenttype)
            {
                FileDownloadName = Path.GetFileName(Downloadfilepath)
            };
        }
    }
}
