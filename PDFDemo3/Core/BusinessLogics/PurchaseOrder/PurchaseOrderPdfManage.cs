﻿using IronPdf.Signing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using PDFDemo3.Core.BusinessModels.PurchaseOrder;
using PDFDemo3.SeedWorks;
using PDFDemo3.Services.Controllers;
using System.Reflection.PortableExecutable;

namespace PDFDemo3.Core.BusinessLogics.PurchaseOrder
{
    public class PurchaseOrderPdfManage
    {
        readonly IRazorViewToStringRenderer razorViewToStringRenderer;

        public PurchaseOrderPdfManage(IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            this.razorViewToStringRenderer = razorViewToStringRenderer;
        }
        public async Task<IActionResult> GeneratePdfPurchaseOrder(PurchaseOrderRequest request)
        {
            string PDFfilename = DateTime.Now.Ticks.ToString() + ".pdf";
            var headerHtml = await razorViewToStringRenderer.RenderViewToStringAsync("SeedWorks/Templates/PurchaseOrder/Header.cshtml", request);
            var footerHtml = await System.IO.File.ReadAllTextAsync("SeedWorks/Templates/PurchaseOrder/Footer.cshtml");

            var renderer = new ChromePdfRenderer();
			renderer.RenderingOptions.MarginTop = 155;
            renderer.RenderingOptions.MarginLeft = 5;
            renderer.RenderingOptions.MarginRight = 5;
            //renderer.RenderingOptions.MarginBottom = 41;
            renderer.RenderingOptions.HtmlHeader = new HtmlHeaderFooter()
            {
                HtmlFragment = headerHtml,
                MaxHeight = 500
            };
            //renderer.RenderingOptions.HtmlFooter = new HtmlHeaderFooter()
            //{
            //    HtmlFragment = footerHtml,
            //    MaxHeight = 100,
            //};
            renderer.RenderingOptions.PaperFit.UseResponsiveCssRendering(1100);

            var html = await razorViewToStringRenderer.RenderViewToStringAsync("SeedWorks/Templates/PurchaseOrder/PurchaseOrder.cshtml", request);
            var pdf = await renderer.RenderHtmlAsPdfAsync(html);
            //pdf.MetaData.Title = "Title";
            //pdf.MetaData.Author = "Author";
            //pdf.MetaData.Creator = "Creator";

            byte[] attachment = File.ReadAllBytes("SeedWorks/Assets/Attachments/lab2.docx");
            pdf.Attachments.AddAttachment("lab2.docx", attachment);

            //Add Footer
            //var footer = new HtmlHeaderFooter();
            //footer.HtmlFragment = footerHtml;
            //var lastPageIndex = new List<int>() { pdf.PageCount - 1 };
            //pdf.AddHtmlFooters(footer, 1, lastPageIndex);

            if (request.Watermark != null)
            {
                string watermarkHtml = "<H1>" + request.Watermark + "</H1>";
                pdf.ApplyWatermark(watermarkHtml, rotation: -45, opacity: 50);
            }
            //pdf.SecuritySettings.OwnerPassword = "123"; // password to edit the pdf
            //pdf.SecuritySettings.UserPassword = "456"; // password to open the pdf
            //pdf.SecuritySettings.AllowUserCopyPasteContent = false;
            //pdf.SecuritySettings.AllowUserCopyPasteContentForAccessibility = false;
            //pdf.SecuritySettings.AllowUserEdits = IronPdf.Security.PdfEditSecurity.NoEdit;
            //pdf.SecuritySettings.AllowUserPrinting = IronPdf.Security.PdfPrintSecurity.NoPrint;
            //pdf.SecuritySettings.AllowUserAnnotations = false;
            //pdf.SecuritySettings.AllowUserFormData = false;

            //pdf.SecuritySettings.MakePdfDocumentReadOnly("123"); //print only         

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "TempFilePDF", PDFfilename);
            pdf.SaveAs(filePath); //PDFA-1B
														  //  pdf.SaveAsPdfA(filePath); //PDFA-3A

			var sig = new PdfSignature("SeedWorks/Assets/Sign/Arthit.pfx", "Arthit@2545");
            //var sig = new PdfSignature("SeedWorks/Assets/Sign/NasanTestSignature.pfx", "123456");
            //sig.SignatureDate = new DateTime(2000, 12, 02);
            //sig.SigningContact = "IronSoftware";
            //sig.SigningLocation = "Chicago";
            //sig.SigningReason = "How to guide";
            //sig.TimestampHashAlgorithm = TimestampHashAlgorithms.SHA256;
            //sig.TimeStampUrl = "http://timestamp.digicert.com";
            //sig.SignatureImage = new PdfSignatureImage("SeedWorks/Assets/Img/Signature02.png", 0, new Rectangle(0, 0, 100, 100));
            //sig.SignPdfFile(filePath);

            return await DownloadFile(PDFfilename);
        }
        private async Task<IActionResult> DownloadFile(string filename)
        {
            var Downloadfilepath = Path.Combine(Directory.GetCurrentDirectory(), "TempFilePDF", filename);

            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(Downloadfilepath, out var contenttype))
            {
                contenttype = "application/octet-stream";
            }

            var bytes = await System.IO.File.ReadAllBytesAsync(Downloadfilepath);
            return new FileContentResult(bytes, contenttype)
            {
                FileDownloadName = Path.GetFileName(Downloadfilepath)
            };
        }
    }
}
