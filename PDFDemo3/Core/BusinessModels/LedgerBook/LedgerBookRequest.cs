﻿using PDFDemo3.Core.BusinessModels.PurchaseOrder;
using PDFDemo3.SeedWorks.Constants;

namespace PDFDemo3.Core.BusinessModels.LedgerBook
{
	public class LedgerBookRequest
	{
        public LanguageId LanguageId { get; set; }
        public string Company { get; set; } = null!;
        public string ExporterName { get; set; } = null!;
        private DateTime _ExportDate; // backing field

        public DateTime ExportDate
        {
            get { return _ExportDate; }
            set { _ExportDate = value; }
        }
        public string ExportDateString => _ExportDate.ToString("dd/MM/yyyy");
        public string? Watermark { get; set; }
		public List<Account> Accounts { get; set; } = null!;
	}
    public class Account
    {
        public string code { get; set; } = null!;
        public string name { get; set; } = null!;
        public List<AccountItem> AccountItems { get; set; } = null!;
    }

    public class AccountItem
    {
        public string branch { get; set; } = null!;

        private DateTime _date; // backing field

        public DateTime date
        {
            get { return _date; }
            set { _date = value; }
        }
        public string date_string => _date.ToString("dd/MM/yyyy");
        public string doc_num { get; set; } = null!;
        public string? description { get; set; }
        public string? doc_ref_first { get; set; }
        public string? doc_ref_second { get; set; }
        public double debit { get; set; }
        public double credit { get; set; }
        public double balance { get; set; }
    }
}