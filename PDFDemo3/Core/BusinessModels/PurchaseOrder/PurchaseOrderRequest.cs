﻿namespace PDFDemo3.Core.BusinessModels.PurchaseOrder  
{
    public class PurchaseOrderRequest
    {
        public string DocNum { get; set; } = null!;
        public string? Watermark { get; set; }
        public List<OrderItem> OderItems { get; set; } = null!;
    }
    public class OrderItem
    {
        public string type { get; set; } = null!; //ประเภท
        public string name { get; set; } = null!; //รายการ
        public int qty { get; set; } //จำนวน
        public string unit { get; set; } = null!; //หน่วยนับ
        public double price { get; set; } //ราคา
        public double pre_price { get; set; } //ราคาก่อนภาษี
        public int discount { get; set; } //ส่วนลด
    }
}
