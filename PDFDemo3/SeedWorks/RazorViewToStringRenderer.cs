﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace PDFDemo3.SeedWorks
{
    public class RazorViewToStringRenderer : IRazorViewToStringRenderer
    {
        private IRazorViewEngine _viewEngine;

        private ITempDataProvider _tempDataProvider;

        private IServiceProvider _serviceProvider;

        public RazorViewToStringRenderer(IRazorViewEngine viewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider)
        {
            _viewEngine = viewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
        }

        public async Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model)
        {
            ActionContext actionContext = GetActionContext();
            IView view = FindView(actionContext, viewName);
            using StringWriter output = new StringWriter();
            ViewContext viewContext = new ViewContext(actionContext, view, new ViewDataDictionary<TModel>(new EmptyModelMetadataProvider(), new ModelStateDictionary())
            {
                Model = model
            }, new TempDataDictionary(actionContext.HttpContext, _tempDataProvider), output, new HtmlHelperOptions());
            await view.RenderAsync(viewContext);
            return output.ToString();
        }
        private ActionContext GetActionContext()
        {
            DefaultHttpContext defaultHttpContext = new DefaultHttpContext();
            defaultHttpContext.RequestServices = _serviceProvider;
            return new ActionContext(defaultHttpContext, new RouteData(), new ActionDescriptor());
        }

        private IView FindView(ActionContext actionContext, string viewName)
        {
            ViewEngineResult view = _viewEngine.GetView(null, viewName, isMainPage: true);
            if (view.Success)
            {
                return view.View;
            }

            ViewEngineResult viewEngineResult = _viewEngine.FindView(actionContext, viewName, isMainPage: true);
            if (viewEngineResult.Success)
            {
                return viewEngineResult.View;
            }

            IEnumerable<string> second = view.SearchedLocations.Concat(viewEngineResult.SearchedLocations);
            string message = string.Join(Environment.NewLine, new string[1] { "Unable to find view '" + viewName + "'. The following locations were searched:" }.Concat(second));
            throw new InvalidOperationException(message);
        }
    }
}

