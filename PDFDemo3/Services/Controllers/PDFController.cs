﻿using Microsoft.AspNetCore.Mvc;
using PDFDemo3.Core.BusinessLogics.PurchaseOrder;
using PDFDemo3.Core.BusinessModels.LedgerBook;
using PDFDemo3.Core.BusinessModels.PurchaseOrder;

namespace PDFDemo3.Services.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class PDFController : ControllerBase
    {
        readonly PurchaseOrderPdfManage purchaseOrderPdfManage;
        readonly LedgerBookPdfManage ledgerBookPdfManage;

        public PDFController(PurchaseOrderPdfManage purchaseOrderPdfManage, LedgerBookPdfManage ledgerBookPdfManage)
        {
            this.purchaseOrderPdfManage = purchaseOrderPdfManage;
            this.ledgerBookPdfManage = ledgerBookPdfManage;
        }

        [HttpPost]
        public async Task<IActionResult> GeneratePDFPurchaseOrder([FromBody] PurchaseOrderRequest request)
        {
            var result = await purchaseOrderPdfManage.GeneratePdfPurchaseOrder(request);
            return result;
        }

		[HttpPost]
		public async Task<IActionResult> GeneratePDFLedgerBook([FromBody] LedgerBookRequest request)
		{
			var result = await ledgerBookPdfManage.GeneratePdfLedgerBook(request);
			return result;
		}
	}
}
